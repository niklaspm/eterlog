# Eterlog

## Example

```go
func main() {
    connectionString := "mongodb://user:pass@host:port/?authSource=admin&ssl=false"
    defaultDatabase := "logging"
	defaultCollection := "ebg"
    defaults := bson.D{
		{Key: "service", Value: "eterlog"},
    }

    logger, err := GetLogger(connectionString, defaultDatabase, defaultCollection, defaults)
    if err != nil {
        panic(err)
    }

    // .Info - .Error - .Panic
    logger.Info("could not verify jwt token", bson.D{
            {"token", "ijt843jfidsj8wejf"},
            {"requestId", "38dla10"},
        }, "panic")
}
```