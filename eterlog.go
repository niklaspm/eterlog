package Eterlog

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

type Eterlogger struct {
	Client            *mongo.Client
	Context           context.Context
	DefaultDatabase   string
	DefaultCollection string
	Defaults          bson.D
	DebugToStdOut	  bool
}

func GetLogger(connectionString string, database string, defaultCollection string, defaults bson.D, debugToStdOut bool) (logger Eterlogger, err error) {
	// Create client
	logger.Client, err = mongo.NewClient(options.Client().ApplyURI(connectionString))
	if err != nil {
		panic(err)
	}

	// Connect
	logger.Context = context.Background()
	err = logger.Client.Connect(logger.Context)
	if err != nil {
		panic(err)
	}

	// Set defaults
	logger.Defaults = defaults
	logger.DefaultDatabase = database
	logger.DefaultCollection = defaultCollection
	logger.DebugToStdOut = debugToStdOut

	return
}

func (logger Eterlogger) chain(message string, level string, context bson.D) (entry bson.D) {
	// Initialize log entry
	entry = bson.D{
		{Key: "time", Value: time.Now().String()},
		{Key: "unix", Value: time.Now().Unix()},
		{Key: "level", Value: level},
		{Key: "message", Value: message},
	}
	// Chain defaults
	for _, p := range logger.Defaults {
		entry = append(entry, bson.E{
			Key: p.Key, Value: p.Value,
		})
	}

	// Chain context
	_context := bson.D{}
	for _, p := range context {
		_context = append(_context, bson.E{
			Key: p.Key, Value: p.Value,
		})
	}

	return append(entry, bson.E{Key: "properties", Value: _context})
}

func (logger Eterlogger) Log(message string, context bson.D, level string, collection []string) {
	var col string
	// Check for custom collection target
	if len(collection) == 0 {
		col = logger.DefaultCollection
	} else {
		col = collection[0]
	}

	// Fetch log entry
	entry := logger.chain(message, level, context)

	if(logger.DebugToStdOut) {
		println(message)
	}

	// Put
	_, err := logger.Client.Database(logger.DefaultDatabase).Collection(col).InsertOne(logger.Context, entry)
	if err != nil {
		panic(err)
	}

	// Panic if panic
	if level == "panic" {
		panic(message)
	}
}

func (logger Eterlogger) Info(message string, context bson.D, collection ...string) {
	logger.Log(message, context, "info", collection)
}

func (logger Eterlogger) Error(message string, context bson.D, collection ...string) {
	logger.Log(message, context, "error", collection)
}

func (logger Eterlogger) Panic(message string, context bson.D, collection ...string) {
	logger.Log(message, context, "panic", collection)
}
